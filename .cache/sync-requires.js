const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---src-pages-404-jsx": hot(preferDefault(require("/home/vale/Repos/margit.2020/src/pages/404.jsx"))),
  "component---src-pages-index-jsx": hot(preferDefault(require("/home/vale/Repos/margit.2020/src/pages/index.jsx")))
}

