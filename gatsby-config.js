module.exports = {
  pathPrefix: '/gatsby',
  siteMetadata: {
    title: 'Margit Birthday 2020 Mixtape',
  },
  plugins: ['gatsby-plugin-postcss'],
};
