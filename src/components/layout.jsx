import React from 'react';
import PropTypes from 'prop-types';

import './index.css';
import './background.css';

const TemplateWrapper = ({ children }) => (
  <>
    {children}
  </>
);

TemplateWrapper.propTypes = {
  children: PropTypes.node,
};

TemplateWrapper.defaultProps = {
  children: null,
};

export default TemplateWrapper;
