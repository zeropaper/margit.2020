/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';

import Layout from '../components/layout';

import './cassette.css';

const shuffle = (array) => {
  let currentIndex = array.length;
  let temporaryValue;
  let randomIndex;

  // While there remain elements to shuffle...
  while (currentIndex !== 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    // eslint-disable-next-line no-param-reassign
    array[currentIndex] = array[randomIndex];
    // eslint-disable-next-line no-param-reassign
    array[randomIndex] = temporaryValue;
  }

  return array;
};

const tracks = [
  // Kristi
  'DGMdNgBVn4Q',
  'oxq6U2DG2AY',
  'ezMvJISJ4I4',
  'zgonWgD083o',
  'hunha8UtqCk',
  'ilottRbDnGY',
  // vale
  // 'veiJLhXdwn8',
  'O52jAYa4Pm8',
  'FpGI9gzI5D4',
  'M7X6oYg6iro',
  'wxi66Wm0XQw',
  '-SKqJdZ6WCk',
  '-TM0akYA1yI',
  'yEvWNu4KUbQ',
  // Joolz
  'qSPnmUWADAM',
  'j5BLHeOdvYI',
  'y7mwZULsVcQ',
  'lkbm-QUo4rM',
  'C_d9ZFXwxvM',
  '0ScYz9sNaQk',
  'Y21VecIIdBI',
  'ZxcCN6BgO_k',
];


const IndexPage = () => {
  const [visible, setVisibility] = React.useState(false);

  const src = `https://www.youtube.com/embed/?playlist=${shuffle(tracks).join(',')}&version=3&autoplay=1&enablejsapi=1&modestbranding=1&color=white&iv_load_policy=3`;

  return (
    <Layout>
      <div id="holder">
        {/* <div
          id="cassette"
          className={visible && 'flipped'}
          onClick={() => setVisibility(!visible)}
        >
          {' '}
        </div> */}

        <iframe
          // className={visible && 'flipped'}
          title="player"
          id="ytplayer"
          type="text/html"
          width="720"
          height="405"
          src={src}
          frameBorder="0"
          allowFullScreen
        >
          {' '}
        </iframe>

      </div>
    </Layout>
  );
};

export default IndexPage;
